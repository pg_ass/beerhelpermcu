// BlockRegister.h

#ifndef _BLOCKREGISTER_h
#define _BLOCKREGISTER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define SOFT_REV_ID         2

#define ADDR_REV_ID         0x01
#define ADDR_CURR_TEMP      0x02
#define ADDR_RTC_TIME       0x03
#define ADDR_TIME_PAUSE_1   0x04
#define ADDR_TEMP_PAUSE_1   0x05
#define ADDR_TIME_PAUSE_2   0x06
#define ADDR_TEMP_PAUSE_2   0x07
#define ADDR_TIME_PAUSE_3   0x08
#define ADDR_TEMP_PAUSE_3   0x09
#define ADDR_TIME_PAUSE_4   0x0A
#define ADDR_TEMP_PAUSE_4   0x0B
#define ADDR_TIME_PAUSE_5   0x0C
#define ADDR_TEMP_PAUSE_5   0x0D
#define ADDR_TIME_PAUSE_6   0x0E
#define ADDR_TEMP_PAUSE_6   0x0F
#define ADDR_TIME_PAUSE_7   0x10
#define ADDR_TEMP_PAUSE_7   0x11
#define ADDR_TIME_PAUSE_8   0x12
#define ADDR_TEMP_PAUSE_8   0x13
#define ADDR_TIME_PAUSE_9   0x14
#define ADDR_TEMP_PAUSE_9   0x15
#define ADDR_TIME_PAUSE_10  0x16
#define ADDR_TEMP_PAUSE_10  0x17
#define ADDR_FULL_COOK_TIME 0x18
#define ADDR_CURR_COOK_TIME 0x19
#define MANUAL_CONTROL      0x1A


class BlockRegisterClass
{
 protected:

     volatile int  const rev_id = SOFT_REV_ID;
     volatile int  curr_temp;
     volatile int  curr_temp_real;

 public:

    volatile bool manual;
    volatile bool power_en;

	void init();
    int write(int addr, int param);
    int read(int addr);
};

extern BlockRegisterClass BlockRegister;

#endif

