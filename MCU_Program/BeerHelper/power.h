#ifndef power_h
    #define power_h

    #include "Arduino.h"

    class Power
    {
    public:
        Power(int pin);
        void enable();
        void disable();
    private:
        int _pin;
    };

#endif
