#include "power.h"

Power::Power(int pin)
{
    _pin = pin;
    pinMode(_pin, OUTPUT);
    digitalWrite(_pin, 1);
}

void Power::enable()
{
    digitalWrite(_pin, 0);
}
void Power::disable()
{
    digitalWrite(_pin, 1);
}
