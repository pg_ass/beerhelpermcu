#include "BlockRegister.h"
#include "power.h"
#include <MsTimer2.h>

#define DEBUG_PARSER    1

#define POWER_PIN       7
#define USART_INT_RX    21
#define SOP             0x20
#define EOP             0x80
#define COMMAND         0xDE
#define REG_WRITE       0x05
#define REG_READ        0x07

Power pwr(POWER_PIN);
BlockRegisterClass reg;

void setup()
{
    pwr.disable();
    Serial.begin(9600);
    MsTimer2::set(1000, timerEvent);
    //MsTimer2::start();
    sei();
}

void loop()
{
}

void serialEvent()
{
    int state = 0;
    if (Serial.available() >= 9)
        state = cmdParser();
    if (state != 0)
        clear_rx_buffer();
}

void timerEvent()
{
    double t;
    t = analogRead(2);
    t = t * (5.0 / 1023);
    t = (5.0 * 2980 / t) - 2980;
    Serial.print("Temperature: ");
    Serial.println(t);
}

void clear_rx_buffer()
{
    Serial.flush();
}

int cmdParser()
{
    int ch;
    int address;
    int params;
    int cmd_type;

    ch = Serial.read();
#if DEBUG_PARSER == 1
    if (ch != SOP)
    {
        Serial.write("Err start of packet\n");
        return -1;
    }
    else
    {
        Serial.write("Good start of packet\n");
    }
#else
    if (ch != SOP)
    {
        return -1;
    }
#endif


    ch = Serial.read();
#if DEBUG_PARSER == 1
    if (ch != COMMAND)
    {
        Serial.write("Err payload type\n");
        return -1;
    }
    else
    {
        Serial.write("Good payload type\n");
    }
#else
    if (ch != COMMAND)
    {
        return -1;
    }
#endif

    ch = Serial.read();
    cmd_type = ch;
#if DEBUG_PARSER == 1
    if (ch != REG_READ && ch != REG_WRITE)
    {
        Serial.write("Err command id\n");
        return -1;
    }
    else
    {
        Serial.write("Good command id\n");
    }
#else
    if (ch != REG_READ && ch != REG_WRITE)
    {
        return -1;
    }
#endif

    ch = Serial.read();
    address = ch;
#if DEBUG_PARSER == 1
    Serial.write("Get address: ");
    Serial.write(address);
    Serial.write("\n");
#endif

    for (int i = 0; i < 4; i++)
    {
        ch = Serial.read();
        params = (params << i * 8) | (ch & 0xFF);
    }
#if DEBUG_PARSER == 1
    Serial.write("Get parameter: ");
    Serial.write(params);
    Serial.write("\n");
#endif

    ch = Serial.read();
#if DEBUG_PARSER == 1
    if (ch != EOP)
    {
        Serial.write("Err end of packet\n");
        return -1;
    }
    else
    {
        Serial.write("Good end of packet\n");
    }
#endif

    if (cmd_type == REG_WRITE)
    {
        ch = reg.write(address, params);
        if (ch == -1)
        {
            Serial.write("Err write address");
        }
    }
    else if (cmd_type == REG_READ)
    {
        ch = reg.read(address);
        if (ch == -1)
        {
            Serial.write("Err read address");
        }
        else
        {
            Serial.write(ch);
            Serial.write("\n");
        }
    }

    Serial.write("==================================================\n");
    power_analyse();
    return 0;
}

void power_analyse()
{

    if (reg.manual == true && reg.power_en == true)
    {
        pwr.enable();
        //Serial.write("Power enable!");
    }
    else
    {
        pwr.disable();
        //Serial.write("Power disable!");
    }
}