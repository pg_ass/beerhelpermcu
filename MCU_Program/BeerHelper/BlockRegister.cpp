// 
// 
// 

#include "BlockRegister.h"


void BlockRegisterClass::init()
{
    curr_temp = 0;
    curr_temp_real = 0;
}

int BlockRegisterClass::write(int addr, int param)
{
    switch (addr)
    {
    case ADDR_CURR_TEMP:
        curr_temp = param;
        break;
    case MANUAL_CONTROL:
        manual = param & 0x1;
        power_en = (param >> 1) & 0x1;
        break;
    default:
        return -1;
    }

    return 0;
}


int BlockRegisterClass::read(int addr)
{
    switch (addr)
    {
    case ADDR_REV_ID:
        return rev_id;
    case ADDR_CURR_TEMP:
        return curr_temp_real;
    case MANUAL_CONTROL:
        return (power_en << 1) | manual;
    default:
        return -1;
    }
}


BlockRegisterClass BlockRegister;

